<?php

namespace Drupal\mammoth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Render\FormattableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\file\FileRepository;

/**
 * Returns responses for mammoth routes.
 */
class MammothController extends ControllerBase {

  /**
   * Service for accessing filesystem.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;
  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * MammothController constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\file\FileRepository $fileRepository
   *   The file repository service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file URL generator service.
   */
  public function __construct(FileSystemInterface $fileSystem, EntityTypeManagerInterface $entityTypeManager, FileRepository $fileRepository, FileUrlGeneratorInterface $fileUrlGenerator) {
    $this->fileSystem = $fileSystem;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileRepository = $fileRepository;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Get the necessary services from the container.
    $fileSystem = $container->get('file_system');
    $entityTypeManager = $container->get('entity_type.manager');
    $fileRepository = $container->get('file.repository');
    $fileUrlGenerator = $container->get('file_url_generator');

    // Return a new instance of the class with the injected services.
    return new static($fileSystem, $entityTypeManager, $fileRepository, $fileUrlGenerator);
  }

  /**
   * Presents the feeds feed creation form.
   *
   * @return array
   *   A form array as expected by drupal_render().
   *
   * @todo When node page gets converted, look at the implementation.
   */
  public function word2htmlPage() {
    $output = "";
    $template = <<<EOT

    <div class="container">
      <input id="document" type="file" />
	  
	  <input type="button" id="copy-btn" value=":copy_html" />
      <div class="row">
        <div class="span8">
          <div id="output" class="well">
          </div>
		  
		  <textarea cols="20" rows="10" id="output-textarea" ></textarea>
        </div>
        <div class="span4">
          <h3>:messages</h3>
          <div id="messages">
          </div>
        </div>
      </div>
	</div>

EOT;

    $output = new FormattableMarkup($template, [
      ":copy_html" => $this->t("Copy HTML"),
      ":messages" => $this->t("Messages"),
    ]);

    $build = [
      '#markup' => $output,
      '#allowed_tags' => ['div', 'input', 'h3', 'textarea'],
    ];

    $build['#attached']['library'][] = 'mammoth/mammoth';
    $build['#attached']['drupalSettings']['mammoth'] = [
      'basePath' => base_path(),
    ];
    return $build;
  }

  /**
   * Upload Image.
   */
  public function uploadImg(Request $request) {
    $ret = [];

    // Use inputstream module.
    $received = file_get_contents("php://input");
    $content = json_decode($received, TRUE);

    $img_type = $content["imgType"] ?? "";
    $img_buffer = $content["imgBuffer"] ?? "";

    if ($img_type === "" || $img_buffer === "") {
      return new JsonResponse($ret, 201);
    }

    // Prepare file name, dir.
    $img_type_arr = explode("/", $img_type);

    $ext = $img_type_arr[1] ?? "";
    if ($ext === "") {
      return new JsonResponse($ret, 201);
    }

    $decode = base64_decode($img_buffer);

    $file_dir = 'public://mammoth/' . date('Y') . '/' . date('m') . '/' . date('d');
    $return = $this->fileSystem->prepareDirectory($file_dir);
    if (empty($return)) {
      $this->fileSystem->mkdir($file_dir, 0777, TRUE);
    }

    $name = md5($img_buffer);

    $uri = $file_dir . "/" . $name . '.' . $ext;

    // Save file.
    $file = $this->fileRepository->writeData($decode, $uri);

    if (empty($file)) {
      return new JsonResponse($ret, 201);
    }

    $file_uri = $file->getFileUri();
    $file_url = $this->fileUrlGenerator->generateString($file_uri);

    $uuid = $file->uuid();

    $ret = [
      "imgUrl" => $file_url,
      "uuid" => $uuid,
    ];

    return new JsonResponse($ret, 201);
  }

}
