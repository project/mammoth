var basePath = "/";
var tips = "Copy completed, please paste!";
(function($, Drupal, drupalSettings) {
	$(document).ready(function() {
		 $("#document").change(function(e){
			handleFileSelect(e);
		 }); 

       $("#copy-btn").click(function(e){	
	     copyhtml();
       }); 

       basePath = drupalSettings.mammoth.basePath || "/";
	   Drupal.t(tips);
	});
})(jQuery, Drupal, drupalSettings);

function copyhtml(){
	var word_html_ele=document.getElementById("output-textarea");
	word_html_ele.select(); // Select element
	document.execCommand("Copy"); // exec copy command
	alert(tips);
}

var options = {
    convertImage: mammoth.images.imgElement(function(image) {
        return image.read("base64").then(async function(imageBuffer) {
			var imgSrc = "data:" + image.contentType + ";base64," + imageBuffer;
			console.log("basePath",basePath);
			try {
				
				let obj = {
					imgType:image.contentType,
					imgBuffer:imageBuffer
				};
				
				//$.post
                let result = await jQuery.ajax({
                    url: basePath + "mammoth/upload/img",
                    data: JSON.stringify(obj),
                    method: "post",
                    dataType: "json",
                    contentType: 'application/json',
					timeout: 6000
			    });
					
			    let imgUrl = result.imgUrl || "";
				let uuid  = result.uuid || "";
			    //console.log("imgUrl",imgUrl);
			    if(imgUrl !== ""){
				  return {
					src: imgUrl,
					'data-align' : "center",
					alt : "image",
					'data-entity-type' : "file",
					'data-entity-uuid' : uuid
				  };						
			    }else{
				  return {
					src: imgSrc
				  };
			    }	
            } catch (error) {
              //console.error(error);
				return {
					src: imgSrc
				};
          }			  


        });
    })
};

function handleFileSelect(event) {
	readFileInputEventAsArrayBuffer(event, function(arrayBuffer) {
		mammoth.convertToHtml({arrayBuffer: arrayBuffer}, options)
			.then(displayResult)
			.done();
	});
}

function displayResult(result) {
	document.getElementById("output").innerHTML = result.value;
	
	document.getElementById("output-textarea").value = result.value;
	
	var messageHtml = result.messages.map(function(message) {
		return '<li class="' + message.type + '">' + escapeHtml(message.message) + "</li>";
	}).join("");
	
	document.getElementById("messages").innerHTML = "<ul>" + messageHtml + "</ul>";
}

function readFileInputEventAsArrayBuffer(event, callback) {
	var file = event.target.files[0];

	var reader = new FileReader();
	
	reader.onload = function(loadEvent) {
		var arrayBuffer = loadEvent.target.result;
		callback(arrayBuffer);
	};
	
	reader.readAsArrayBuffer(file);
}

function escapeHtml(value) {
	return value
		.replace(/&/g, '&amp;')
		.replace(/"/g, '&quot;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;');
}	

